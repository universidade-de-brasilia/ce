import spacy


class System():
    _language = ""

    def __init__(self, name, language):
        self._name = name
        self._language = language

    def process(self, text):
        if self._language == "pt":
            self._nlp = spacy.load("pt_core_news_sm")

            doc = self._nlp(text)
            entities = []
            part_of_speech = {}

            for ent in doc.ents:
                entities.append(ent)

            for token in doc:
                part_of_speech[token.text] = token.pos_

            return entities, part_of_speech

        elif self._language == "en":
            self._nlp = spacy.load("en_core_web_md")

            doc = self._nlp(text)
            entities = []
            part_of_speech = {}

            for ent in doc.ents:
                entities.append(ent)

            for token in doc:
                part_of_speech[token.text] = token.pos_

            return entities, part_of_speech
