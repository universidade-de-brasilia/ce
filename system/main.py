# coding: utf-8

import time
import json
from experiment import System
from flask import Flask, request, render_template

app = Flask(__name__, template_folder='./templates')

@app.route('/')
def index():
    return render_template("templates/index.html")


@app.route('/request', methods=["GET", "POST"])
@app.route('/request/<language>', methods=["POST"])
def req(language=None):
    request_time_start = time.time()
    request_text = request.form['text']

    if request_text is None:
        return "This endpoint requires text data"

    if language is not None:
        s = System("", language)
        pos = {}
        entities = {}
        process_time = 0
        request_time = 0

        if language == "en":
            s._name = "A"

            start = time.time()
            ents, pos = s.process(request_text)
            end = time.time()

            process_time = end-start

            for ent in ents:
                entities[ent.text] = ent.label_

        elif language == "pt":
            s._name = "B"

            start = time.time()
            ents, pos = s.process(request_text)
            end = time.time()

            process_time = end-start

            for ent in ents:
                entities[ent.text] = ent.label_
        else:
            return "Language not supported"

        # Finishes the HTTP request process
        request_time = time.time() - request_time_start
        message = json.dumps({'system': s._name,
                              'text': request_text,
                              'entities': entities,
                              'part_of_speech': pos,
                              'process_time': process_time,
                              'request_time': request_time})

        return message
    else:
        return request_text
