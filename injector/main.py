import requests
import json
import data


def main():
    # Process the entire data against the system
    dat = data.Data()

    for topic in dat.get("a"):
        for key, value in dat.get("a")[topic].items():
            req = requests.post("http://127.0.0.1:5000/request/en",
                               data={'text': value['text']})

            if req.status_code == 200:
                json_anwser = json.loads(req.text)
                info = {}
                info["topic"] = topic
                info["qt_entities"] = len(json_anwser['entities'].values())
                info["qt_pos"] = len(json_anwser['part_of_speech'].values())
                info["process_time"] = json_anwser['process_time']
                info["request_time"] = json_anwser['request_time']
                information = json.dumps(info)

                f = open("a_" + topic + str(key) + ".json", 'w')
                f.write(information)

    for topic in dat.get("b"):
        for key, value in dat.get("b")[topic].items():
            req = requests.post("http://127.0.0.1:5000/request/pt",
                               data={'text': value['text']})

            if req.status_code == 200:
                json_anwser = json.loads(req.text)
                info = {}
                info["topic"] = topic
                info["qt_entities"] = len(json_anwser['entities'].values())
                info["qt_pos"] = len(json_anwser['part_of_speech'].values())
                info["process_time"] = json_anwser['process_time']
                info["request_time"] = json_anwser['request_time']
                information = json.dumps(info)

                f = open("b_" + topic + str(key) + ".json", 'w')
                f.write(information)


if __name__ == '__main__':
    main()
