# coding: utf-8

system_a = {
    'science': {
        1: {
            'text': 'Mediterranean monk seals are one of the world’s rarest mammals, with less than 700 left.',
            'count': 15,
        },
        2: {
            'text': 'Marine Biologist, Diva Amon investigates the human impact of pollution in the deep sea and how deep sea mining is threatening to destroy valuable habitats.',
            'count': 25,

        },
        3: {
            'text': 'Over-fishing and climate change have caused their prey to move hundreds of miles from their breeding colonies, too far for parents to hunt. To help, local scientists have come up with a plan to create a new colony using fake model penguins to attract the real penguins to a new feeding ground. This clip is from the BBC series, Blue Planet Live. Below are a few ideas to get you started with your class.',
            'count': 74,
        },
    },
    'economy': {
        1: {
            'text': 'One in five of the world’s most valuable companies are impacted by at least 10 in either direction.',
            'count': 18,
        },
        2: {
            'text': 'Carbon-intensive firms are likely to lose 43 percent of their value thanks to policies designed to combat climate change, a report says.',
            'count': 22,

        },
        3: {
            'text': 'But they do echo the warnings issued by the Bank of England governor Mark Carney, who said firms ignoring the climate challenge would go bankrupt. Already some insurance firms are refusing to offer cover to new coal-fired power stations because the risk of policy change is so great. The giant AXA, for instance, says it will stop insuring any new coal construction projects, and totally phase out existing insurance and investments in coal in the EU, by 2030.',
            'count': 78,
        },
    },
    'technology': {
        1: {
            'text': 'Tesla says Autopilot "requires active driver supervision" and drivers should keep their hands on the steering wheel.',
            'count': 17,
        },
        2: {
            'text': 'It said the design of Tesla\'s Autopilot system had allowed the driver to "disengage from the driving task", although it acknowledged the driver had used the technology "in ways inconsistent with guidance and warnings from Tesla".',
            'count': 36,

        },
        3: {
            'text':'The car\'s collision-warning alarm was activated but the car did not brake and hit the fire engine at about 30mph (48km/h). The NTSB said the probable cause of the crash had been the "driver\'s lack of response to the fire truck parked in his lane, due to his inattention and over-reliance on the car\'s advanced driver assistance system".',
            'count': 59,
        },
    }
}

system_b = {
    'science': {
        1: {
            'text': 'O naturalista era capaz de enxergar questões profundas em acontecimentos em que a maioria das pessoas sequer prestaria atenção.',
            'count': 19,
        },
        2: {
            'text': 'Depois de passar cinco anos a bordo do navio Beagle, quando deu a volta ao mundo observando espécies da América do Sul à Oceania, Darwin retornou em 1836 ao Reino Unido, seu país de origem.',
            'count': 35,

        },
        3: {
            'text': 'Darwin tinha problemas de saúde, e só alguém que realmente amasse estudar a natureza teria perseverado como ele fez, ao longo de décadas de experimentos e observações cuidadosas", disse à BBC News Mundo o professor de biologia Ken Thompson, da Universidade de Sheffield, na Inglaterra, autor do livro As plantas mais maravilhosas de Darwin.',
            'count': 54,
        },
    },
    'economy': {
        1: {
            'text': 'Nas últimas décadas, a China exportou quantidades recordes de capital ao restante do mundo.',
            'count': 14,
        },
        2: {
            'text': 'Seus empréstimos diretos e créditos comerciais passaram de quase zero em 1998 para mais de US$ 1,6 trilhão em 2018, cifra que equivale a 2% do PIB mundial.',
            'count': 28,

        },
        3: {
            'text': 'Seus principais braços de investimento são o China Development Bank e o Export-Import Bank of China, através dos quais o gigante asiático se converteu em um importante banqueiro na América Latina. Somados, os cinco países latino-americanos que mais contraíram dívidas com entidades públicas da China devem ao país asiático mais de US\$ 133 bilhões. Para colocar a cifra em perspectiva: isso é mais do que o PIB (Produto Interno Bruto) do Equador.',
            'count': 72,
        },
    },
    'technology': {
        1: {
            'text': 'Os historiadores do cinema se lembrarão dos anos 2010 como a década da Disney.',
            'count': 14,
        },
        2: {
            'text': 'Para aqueles de nós que crescemos lendo histórias em quadrinhos de super-heróis no século 20, foi estranho nossos interesses de nichos se destacarem no entretenimento em massa.',
            'count': 27,

        },
        3: {
            'text': 'A indústria cinematográfica foi mais abalada nos anos 2010 do que em quase qualquer outra década - e as ondas de choque não diminuíram. É difícil dizer se o cinema estará presente no final da década de 2020 e qual será seu formato, se houver. Mas há uma grande chance de que, até 2030, um avatar digital de Marlon Brando tenha estrelado como o Homem-Aranha em uma aventura de realidade virtual transmitida diretamente para um canto do seu cérebro pertencente à Netflix',
            'count': 89,
        },
    }
}


class Data:
    def get(self, system):
        if system == "a":
            return system_a
        elif system == "b":
            return system_b
